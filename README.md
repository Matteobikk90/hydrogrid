# Assignment

The task is to create a React App, in which I will fetch data from a remote server and present it in a form of a chart.

## Use cases

The user lands to the main dahsboard with a default colors filter and the main component already selected.
From there he can change the color schema or load a second chart component.
Both options work fine during the refresh page since the colors are stored into the localstorage and the component is based on the actual url

## How to run

- clone or download the repository
- install the dependencies by running `npm i`
- run `npm start`

## Issues

- Came accross few difficulties with the chart config since I have been working with the "unofficial" chart library that was not supporting all the required settings
