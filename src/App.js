import React, { useState, useEffect } from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Header from './components/Header/Header';
import MainChart from './components/MainChart/MainChart';
import SecondaryChart from './components/SecondaryChart/SecondaryChart';

import './App.scss';

function App() {
  const [colorsType, setColorsType] = useState('original');

  useEffect(() => {
    if (localStorage.getItem('colors') !== null) {
      const settings = localStorage.getItem('colors');
      setColorsType(settings);
    } else {
      localStorage.setItem('colors', 'original');
    }
  }, []);

  const handleSelect = (value) => {
    setColorsType(value);
  };

  return (
    <Router>
      <Header colorsType={colorsType} handleSelect={handleSelect} />
      <main>
        <Switch>
          <Route
            exact
            path="/"
            render={() => <MainChart colorsType={colorsType} />}
          />
          <Route
            path="/secondaryChart"
            render={() => <SecondaryChart colorsType={colorsType} />}
          />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
