import React, { useState } from 'react';

import logo from '../../assets/logo/logo.svg';

import { useHistory } from 'react-router-dom';

function Header(props) {
  const { handleSelect, colorsType } = props;
  let history = useHistory();
  const [toggle, setToggle] = useState(false);
  const [pathname, setPathname] = useState(history.location.pathname);

  const navbarLinks = [
    {
      url: '/',
      title: 'Main',
    },
    {
      url: '/secondaryChart',
      title: 'Secondary',
    },
  ];

  const handleToggle = () => {
    setToggle(!toggle);
  };

  const handleSelectWrapper = (e) => {
    setToggle(!toggle);
    // setToggle(false);
    const { value, name } = e.target;
    if (name === 'pickedComponents') {
      setPathname(value);
      history.push(value);
    } else {
      localStorage.setItem('colors', value);
      handleSelect(value);
    }
  };

  const renderedLinks = navbarLinks.map((navbarLink, i) => {
    return (
      <option key={i} value={navbarLink.url}>
        {navbarLink.title}
      </option>
    );
  });

  return (
    <header>
      <img src={logo} alt="Hydrogrid logo" width="50" />
      <nav className={toggle ? 'active' : null}>
        {/* <nav className={toggle && 'active'}></nav> */}
        <label>
          Colors:
          <select
            value={colorsType}
            onChange={handleSelectWrapper}
            name="pickedColors"
          >
            <option value="original">Version 1 (original colors)</option>
            <option value="random">Version 2 (random colors)</option>
          </select>
        </label>
        <label>
          Components:
          <select
            value={pathname}
            onChange={handleSelectWrapper}
            name="pickedComponents"
          >
            {renderedLinks}
          </select>
        </label>
      </nav>
      <button onClick={handleToggle} className={toggle ? 'active' : null}>
        <span></span>
        <span></span>
        <span></span>
      </button>
    </header>
  );
}

export default Header;
