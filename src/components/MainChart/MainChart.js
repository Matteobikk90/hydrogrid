import React, { Fragment, useState, useEffect } from 'react';

import ChartsService from '../../services/chart.service';
import ColorsService from '../../services/colorsService.service';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
require('highcharts/modules/exporting')(Highcharts);

function MainChart(props) {
  const { colorsType } = props;
  let actualData = [];
  let planData = [];
  let efficiencyData = [];
  let formattedTimestampData = [];
  let maximumData = [];
  let minimumData = [];
  let config = {};

  const [mainChartData, setMainChartData] = useState([]);
  const [visible, setVisible] = useState(false);

  const pickedColors = ColorsService.generateRandomColors(colorsType);

  useEffect(() => {
    ChartsService.getMainChart()
      .then((response) => {
        setMainChartData(response.data);
        setVisible(true);
      })
      .catch((err) => console.log(err.message));
  }, []);
  // }, [visible]);

  const formatDate = (date) => {
    return `${date.getUTCDate()}/${
      date.getMonth() + 1
    }/${date.getUTCFullYear()}`;
  };

  mainChartData.map((el) => {
    const { actual, efficiency, maximum, minimum, plan, timestamp } = el;

    actualData.push(actual);
    efficiencyData.push(efficiency);
    planData.push(plan);
    minimumData.push(minimum);
    maximumData.push(maximum);
    formattedTimestampData.push(formatDate(new Date(timestamp)));
    return null;
  });

  const minPlanData = Math.min(...actualData);
  const maxPlanData = Math.max(...actualData);
  const minEfficiencyData = Math.min(...efficiencyData);
  const maxEfficiencyData = Math.max(...efficiencyData);
  const minMinimumData = Math.min(...minimumData);
  const maxMaximumData = Math.max(...maximumData);

  config = {
    title: {
      text: 'Main Chart',
    },
    // Bottom legend with circle icons
    legend: {
      symbolHeight: 12,
      symbolWidth: 12,
      symbolRadius: 25,
      verticalAlign: 'bottom',
    },
    plotOptions: {
      line: {
        marker: {
          enabled: false,
        },
      },
    },
    // Array of y axis with title(MW and Efficiency %) and Minimum and maximum
    yAxis: [
      {
        // Grey lines for minimum power and maximum power
        plotLines: [
          {
            value: maxMaximumData,
            color: pickedColors.minMax,
            zIndex: 5,
            label: {
              text: `Maximum Power ${maxMaximumData}`,
              align: 'right',
            },
          },
        ],
        min: minPlanData,
        max: maxPlanData,
        title: {
          text: 'MW',
        },
      },
      {
        plotLines: [
          {
            value: minMinimumData,
            color: pickedColors.minMax,
            dashStyle: 'shortdot',
            zIndex: 5,
            width: 1,
            label: {
              text: `Minimum Power ${minMinimumData}`,
              align: 'right',
            },
          },
        ],
        min: minEfficiencyData,
        max: maxEfficiencyData,
        title: {
          text: 'Efficiency %',
          style: {
            color: pickedColors.efficiency,
          },
        },
        labels: {
          style: {
            color: pickedColors.efficiency,
          },
        },
        opposite: true,
      },
    ],
    // Dotted line of current mouse position
    xAxis: {
      categories: formattedTimestampData,
      crosshair: {
        width: 2,
        color: 'gray',
        dashStyle: 'shortdot',
        label: true,
      },
    },
    // Show all details of current mouse position
    tooltip: {
      shared: true,
    },
    // Click into chart to zoom that section(then you can reset)
    chart: {
      zoomType: 'x',
    },
    // Exporting buttons
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            'viewFullscreen',
            'downloadPNG',
            'downloadJPEG',
            'downloadPDF',
          ],
        },
      },
    },
    // Actual data
    series: [
      {
        name: 'Efficiency',
        data: efficiencyData,
        color: pickedColors.efficiency,
        yAxis: 1,
      },
      {
        name: 'Actual',
        data: actualData,
        color: pickedColors.actual,
        yAxis: 0,
        type: 'column',
      },
      {
        name: 'Plan',
        data: planData,
        color: pickedColors.plan,
        yAxis: 0,
        type: 'column',
      },
      {
        name: 'Maximum',
        data: maximumData,
        color: pickedColors.minMax,
        showInLegend: false,
      },
      {
        name: 'minimum',
        data: minimumData,
        color: pickedColors.minMax,
        showInLegend: false,
      },
    ],
  };

  return (
    <Fragment>
      {visible && (
        <HighchartsReact
          options={config}
          highcharts={Highcharts}
        ></HighchartsReact>
      )}
    </Fragment>
  );
}

export default MainChart;
