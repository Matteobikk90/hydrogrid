import React, { Fragment, useState, useEffect } from 'react';

import ChartsService from '../../services/chart.service';
import ColorsService from '../../services/colorsService.service';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
require('highcharts/modules/exporting')(Highcharts);

function SecondaryChart(props) {
  const { colorsType } = props;
  let actualData = [];
  let planData = [];
  let efficiencyData = [];
  let formattedTimestampData = [];
  let maximumData = [];
  let minimumData = [];
  let config = {};

  const [secondaryChartData, setSecondaryChartData] = useState([]);
  const [visible, setVisible] = useState(false);

  const pickedColors = ColorsService.generateRandomColors(colorsType);

  useEffect(() => {
    ChartsService.getSecondaryChart()
      .then((response) => {
        setSecondaryChartData(response.data);
        setVisible(true);
      })
      .catch((err) => console.log(err.message));
  }, []);

  const formatDate = (date) => {
    return `${date.getUTCDate()}/${
      date.getMonth() + 1
    }/${date.getUTCFullYear()}`;
  };

  secondaryChartData.map((el) => {
    const { actual, efficiency, maximum, minimum, plan, timestamp } = el;

    actualData.push(actual);
    efficiencyData.push(efficiency);
    planData.push(plan);
    maximumData.push(maximum);
    minimumData.push(minimum);
    formattedTimestampData.push(formatDate(new Date(timestamp)));
    return null;
  });

  const minPlanData = Math.min(...actualData);
  const maxPlanData = Math.max(...actualData);
  const minEfficiencyData = Math.min(...efficiencyData);
  const maxEfficiencyData = Math.max(...efficiencyData);
  const minMinimumData = Math.min(...minimumData);
  const maxMaximumData = Math.max(...maximumData);

  config = {
    title: {
      text: 'Secondary Chart',
    },
    legend: {
      symbolHeight: 12,
      symbolWidth: 12,
      symbolRadius: 25,
      verticalAlign: 'bottom',
    },
    plotOptions: {
      line: {
        marker: {
          enabled: false,
        },
      },
    },
    yAxis: [
      {
        plotLines: [
          {
            value: maxMaximumData,
            color: pickedColors.minMax,
            zIndex: 5,
            label: {
              text: `Maximum Power ${maxMaximumData}`,
              align: 'right',
            },
          },
        ],
        min: minPlanData,
        max: maxPlanData,
        title: {
          text: 'MW',
        },
      },
      {
        plotLines: [
          {
            value: minMinimumData,
            color: pickedColors.minMax,
            dashStyle: 'shortdot',
            zIndex: 5,
            width: 1,
            label: {
              text: `Minimum Power ${minMinimumData}`,
              align: 'right',
            },
          },
        ],
        min: minEfficiencyData,
        max: maxEfficiencyData,
        title: {
          text: 'Efficiency %',
          style: {
            color: pickedColors.efficiency,
          },
        },
        labels: {
          style: {
            color: pickedColors.efficiency,
          },
        },
        opposite: true,
      },
    ],
    xAxis: {
      categories: formattedTimestampData,
      crosshair: {
        width: 2,
        color: 'gray',
        dashStyle: 'shortdot',
        label: true,
      },
    },
    tooltip: {
      shared: true,
    },
    chart: {
      zoomType: 'x',
    },
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            'viewFullscreen',
            'downloadPNG',
            'downloadJPEG',
            'downloadPDF',
          ],
        },
      },
    },
    series: [
      {
        name: 'Efficiency',
        data: efficiencyData,
        color: pickedColors.efficiency,
        yAxis: 1,
      },
      {
        name: 'Actual',
        data: actualData,
        color: pickedColors.actual,
        yAxis: 0,
        type: 'column',
      },
      {
        name: 'Plan',
        data: planData,
        color: pickedColors.plan,
        yAxis: 0,
        type: 'column',
      },
      {
        name: 'Maximum',
        data: maximumData,
        color: pickedColors.minMax,
        showInLegend: false,
      },
      {
        name: 'minimum',
        data: minimumData,
        color: pickedColors.minMax,
        showInLegend: false,
      },
    ],
  };

  return (
    <Fragment>
      {visible && (
        <HighchartsReact
          options={config}
          highcharts={Highcharts}
        ></HighchartsReact>
      )}
    </Fragment>
  );
}

export default SecondaryChart;
