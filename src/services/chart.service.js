import axios from 'axios';

const API_URL_MAIN_CHART =
  'https://run.mocky.io/v3/476d7474-9066-4da5-b40d-8990f09f5f3d';
const API_URL_SECONDARY_CHART =
  'https://run.mocky.io/v3/c83e8d8a-0a8e-44be-bef8-c08bc2837a9c';

class ChartsService {
  getMainChart() {
    return axios
      .get(API_URL_MAIN_CHART)
      .then((response) => {
        return response.data;
      })
      .catch((err) => console.log(err.message));
  }

  getSecondaryChart() {
    return axios
      .get(API_URL_SECONDARY_CHART)
      .then((response) => {
        return response.data;
      })
      .catch((err) => console.log(err.message));
  }
}

export default new ChartsService();
