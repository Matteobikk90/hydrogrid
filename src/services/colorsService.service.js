const ORIGINAL_COLORS = {
  efficiency: '#ad1265',
  minMax: '#4a5860',
  actual: '#0bb7a0',
  plan: '#ccd2d5',
};

class RandomColorsService {
  generateRandomColors = (type) => {
    if (type === 'original') {
      return ORIGINAL_COLORS;
    } else {
      let randomColors = [];
      for (let i = 0; i < 4; i++) {
        randomColors.push(
          `#${Math.floor(Math.random() * 16777215).toString(16)}`
        );
      }
      return randomColors;
    }
  };
}

export default new RandomColorsService();
